/**
 * Aarhus bryghus Tværfagligt projekt 2.semester
 * Andreas Koefoed-Pedersen
 * Nassim Ghonem
 * Emilio Iezzi Højland Agger
 */
package gui;

import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

public class MainApp extends Application {

	public static void main(String[] args) {
		Application.launch(args);
	}

	@Override
	public void start(Stage stage) {
		stage.setTitle("Aarhus Bryghus");
		GridPane pane = new GridPane();
		this.initContent(pane);
		Scene scene = new Scene(pane);
		stage.setScene(scene);
		stage.initStyle(StageStyle.UTILITY);
		stage.setResizable(false);
		stage.setHeight(600);
		stage.setWidth(800);
		stage.show();
	}

	// -------------------------------------------------------------------------
	private void initContent(GridPane pane) {
		pane.setGridLinesVisible(false);
		pane.setPadding(new Insets(20));
		pane.setHgap(30);
		pane.setVgap(30);
		pane.setAlignment(Pos.CENTER);
		//		pane.setStyle("-fx-background-color: #3C4043");

		Button buttonButikPane = new Button("Butikken");
		buttonButikPane.setPrefSize(150, 150);
		pane.add(buttonButikPane, 0, 1);
		buttonButikPane.setOnMouseClicked(e -> butikAction());

		Button buttonLagerPane = new Button("Lageret");
		buttonLagerPane.setPrefSize(150, 150);
		//		buttonLagerPane.setStyle("-fx-background-color: #3C4043");
		pane.add(buttonLagerPane, 1, 1);
		buttonLagerPane.setOnMouseClicked(e -> lagerAction());

		Button buttonStatistikPane = new Button("Statistik");
		buttonStatistikPane.setPrefSize(150, 150);
		pane.add(buttonStatistikPane, 2, 1);
		buttonStatistikPane.setOnMouseClicked(e -> butikAction());
	}

	private void lagerAction() {
	}

	private void butikAction() {
	}

}
