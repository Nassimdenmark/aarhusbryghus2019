package model;

public class Pris {
	private double pris;
	private Produkt produkt;
	private Salgssituation salgssituation;

	public Pris(Produkt produkt, double pris) {
		this.produkt = produkt;
		this.pris = pris;
	}

	public Produkt getProdukt() {
		return this.produkt;
	}

	public Salgssituation getSalgssituation() {
		return this.salgssituation;
	}

	public double getPris() {
		return this.pris;
	}

	@Override
	public String toString() {
		return this.produkt + ", " + this.pris + "kr";
	}
}
