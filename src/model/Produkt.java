package model;

public class Produkt implements Comparable<Produkt> {
	private String navn;
	private int størrelse;
	private ProduktGruppe produktgruppe;
	private Pris pris;

	public Produkt(String navn, int størrelse, ProduktGruppe produktgruppe) {
		this.navn = navn;
		this.størrelse = størrelse;
		this.produktgruppe = produktgruppe;
	}

	public String getNavn() {
		return this.navn;
	}

	public ProduktGruppe getProduktgruppe() {
		return this.produktgruppe;
	}

	public int getStørrelse() {
		return this.størrelse;
	}

	public Pris getPris() {
		return this.pris;
	}

	@Override
	public String toString() {
		return this.navn;

	}

	public int getPrisonSalgssituation() {
		return this.størrelse;

	}

	@Override
	public int compareTo(Produkt produkt) {
		return this.navn.compareTo(produkt.navn);

	}

}
