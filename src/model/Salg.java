package model;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

public class Salg {
	private int ordrenr;
	private LocalDateTime oprettelsesdato;
	private double samletpris;
	private Rabat rabat;
	private boolean isBetalt;
	private final List<Salgslinje> salgslinjer = new ArrayList<>();
	private final List<Kunde> kunder = new ArrayList<>();

	// Constructor med rabat
	public Salg(Rabat rabat) {
		this.ordrenr++;
		this.oprettelsesdato = LocalDateTime.now();
		this.rabat = rabat;
	}

	// Constructor uden parametre
	public Salg() {
		this.ordrenr++;
		this.oprettelsesdato = LocalDateTime.now();
	}

	@Override
	public String toString() {
		return this.ordrenr + " - " + this.oprettelsesdato + " - " + this.samletpris;
	}

	// ------------GET METODER----------------------------
	public boolean isBetalt() {
		return this.isBetalt;
	}

	public void setBetalt(boolean isBetalt) {
		this.isBetalt = isBetalt;
	}

	public void betal(String betalingsType, Salg salg) {
		this.setBetalt(true);
	}

	public double getSamletpris() {
		return this.samletpris;
	}

	public List<Salgslinje> getSalgslinjer() {
		return new ArrayList<>(this.salgslinjer);
	}

	public List<Kunde> getKunder() {
		return new ArrayList<>(this.kunder);
	}

	// ------------CREATE METODER----------------------------

	public Salgslinje createSalgslinje(Produkt produkt, int antal, Pris pris) {
		Salgslinje salgslinje = new Salgslinje(produkt, antal, pris);
		this.salgslinjer.add(salgslinje);
		return salgslinje;
	}

	public Kunde createKunde(int kundeID, String navn, String adresse, int telefonnr, String email) {
		Kunde k = new Kunde(kundeID, navn, adresse, telefonnr, email);
		this.kunder.add(k);
		return k;
	}

	// ------------REMOVE METODER----------------------------

	public Salgslinje deleteSalgslinje(Salgslinje s) {
		if (this.salgslinjer.contains(s)) {
		}
		return s;
	}

	public Salgslinje removeSalgslinje(Salgslinje s) {
		if (this.salgslinjer.contains(s)) {

			this.salgslinjer.remove(s);
			return s;
		} else {
			return null;
		}
	}

	public Kunde deleteKunde(Kunde k) {
		if (this.kunder.contains(k)) {
			this.kunder.remove(k);
		}
		return k;
	}

	public Kunde removeKunde(Kunde k) {
		if (this.kunder.contains(k)) {
			this.kunder.remove(k);

			return k;
		} else {
			return null;
		}
	}

	// ------------------------------------------------
	/**
	 * Metoden bestemmer hvilken implementering af Rabat skal bruges : Strategy
	 * pattern
	 */
	public double useStrategy(int produktPris, int rabatpris) {
		return this.rabat.beregnRabat(produktPris, rabatpris);
	}

	public double beregnPris() {
		for (Salgslinje salgslinje : this.salgslinjer) {

			this.samletpris += salgslinje.getSamletSalgslinjePris();

			this.samletpris = +salgslinje.getSamletSalgslinjePris();

		}
		return this.samletpris;
	}
}
