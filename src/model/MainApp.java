package model;

public class MainApp {

	public static void main(String[] args) {

		//		Kunde k = new Kunde(87, "Adam Bensen", "Arvikavej 65", 97959391, "adben@hotmail.com");
		//		Udlejning u = new Udlejning(LocalDate.parse("2019-10-10"), LocalDate.parse("2019-10-12"), k);
		//
		//		s.createKunde(87, "Adam Bensen", "Arvikavej 65", 97959391, "adben@hotmail.com");
		//		s.createKunde(88, "Charlotte Davidson", "Arvikavej 64", 97959392, "chdav@hotmail.com");
		//		s.createKunde(89, "Erik Flanders", "Arvikavej 63", 97959393, "erfla@hotmail.com");
		//		s.createKunde(90, "Grethe Hansen", "Arvikavej 62", 97959394, "grhan@hotmail.com");
		//		s.createKunde(91, "Ian Jensen", "Arvikavej 61", 97959395, "iajen@hotmail.com");
		//		s.createKunde(92, "Karla Larsen", "Arvikavej 60", 97959396, "kalar@hotmail.com");
		//
		//		System.out.println("flaske: " + flaske);
		//		System.out.println();
		//		System.out.println("flaske produkter: " + flaske.getProdukter());
		//		System.out.println();
		//		System.out.println("s: " + s);
		//		System.out.println();
		//		System.out.println("s salgslinjer: " + s.getSalgslinjer());
		//		System.out.println();
		//		//		System.out.println("p1 pris: " + p1.getPris());
		//		System.out.println();
		//		System.out.println("Kunder: " + s.getKunder());
		//		System.out.println();
		//		System.out.println("u salgslinjer: " + u.getSalgslinjer());
		//		System.out.println();
		//		System.out.println("u start dato: " + u.getStartDato());
		//		System.out.println();
		//		System.out.println("u slut dato: " + u.getSlutDato());
		//		System.out.println();
		//		System.out.println("u pant: " + u.getPantPris());
		//		System.out.println();
		//		System.out.println("Samlet pris: " + s.beregnPris());
		//		System.out.println();
		ProduktGruppe flaske = new ProduktGruppe("Flaske", Enhed.CL);
		ProduktGruppe spiritus = new ProduktGruppe("Spiritus", Enhed.LITER);
		Produkt p1 = new Produkt("Klosterbryg", 60, flaske);
		Produkt p2 = new Produkt("SweetGeorgiaBrown", 60, flaske);
		Produkt p3 = new Produkt("Klosterbryg", 60, flaske);
		flaske.addProdukt(p1);

		System.out.println("flaske: " + flaske);
		System.out.println();
		System.out.println("flaske produkter: " + flaske.getProdukter());
		System.out.println();
		System.out.println("s: " + s);
		System.out.println();
		System.out.println("s salgslinjer: " + s.getSalgslinjer());
		System.out.println();
		System.out.println("Kunder: " + s.getKunder());
		System.out.println();
		System.out.println("u salgslinjer: " + u.getSalgslinjer());
		System.out.println();
		System.out.println("u start dato: " + u.getStartDato());
		System.out.println();
		System.out.println("u slut dato: " + u.getSlutDato());
		System.out.println();
		System.out.println("u pant: " + u.getPantPris());
		System.out.println();
		System.out.println("Samlet pris: " + s.beregnPris());
		System.out.println();
		Salg s = new Salg();

		flaske.addProdukt(p1);
		flaske.addProdukt(p3);
		spiritus.addProdukt(p1);
		spiritus.addProdukt(p2);
		spiritus.addProdukt(p3);

		Salgssituation butik = new Salgssituation("Butik");
		butik.createPris(p1, 36);

		Salgssituation fredagsbar = new Salgssituation("Fredagsbar");
		fredagsbar.createPris(p1, 50);

		//Test
		System.out.println("ProduktGruppe: " + flaske + "\nProdukter: " + flaske.getProdukter() + "\n");
		System.out.println("ProduktGruppe: " + spiritus + "\nProdukter: " + spiritus.getProdukter() + "\n");
		Salg salg = new Salg();
		Pris pris1 = butik.createPris(p1, 36);
		Pris pris2 = fredagsbar.createPris(p1, 50);

		System.out.println(p1);

		salg.createSalgslinje(p1, 1, pris1);
		salg.createSalgslinje(p1, 4, pris2);
		System.out.println(salg.getSalgslinjer().get(0));
		System.out.println(salg.getSalgslinjer().get(1));

	}
}
