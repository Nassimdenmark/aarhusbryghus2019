package model;

import java.util.ArrayList;
import java.util.List;

public class Salgssituation {
	private String navn;
	private List<Pris> priser = new ArrayList<>();

	public Salgssituation(String navn) {
		this.navn = navn;
	}

	public Pris createPris(Produkt produktet, int stkpris) {
		Pris pris = new Pris(produktet, stkpris);
		this.priser.add(pris);
		return pris;
	}

	public void removePris(Pris produkt) {
		if (this.priser.contains(produkt)) {
			this.priser.remove(produkt);
		}
	}

	public ArrayList<Pris> getPriser() {
		return new ArrayList<Pris>(this.priser);
	}

	public double getProduktPris(Produkt produkt) {
		double num = 0;
		for (Pris pris : this.priser) {
			if (pris.getProdukt().equals(produkt)) {
				num = pris.getPris();
			} else {
				System.out.println("Not found");
			}
		}
		return num;
	}

	public String getNavn() {
		return this.navn;
	}

	@Override
	public String toString() {
		return "" + this.navn + " " + this.priser + "\n";
	}

}
