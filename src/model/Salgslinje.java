package model;

public class Salgslinje {
	private int salgslinjenr;
	private int nummer = 1;
	private int antal;
	private double ordrelinjepris;
	private Pris pris;

	public Salgslinje(Produkt produkt, int antal, Pris pris) {
		this.nummer++;
		this.salgslinjenr = this.nummer;
		this.antal = antal;
		this.pris = pris;
	}

	public double getSamletSalgslinjePris() {
		return this.ordrelinjepris * this.antal;
	}

	public int getAntal() {
		return this.antal;
	}

	@Override
	public String toString() {
		return "[ " + "Salgslinje nr " + this.salgslinjenr + ", " + "Antal: " + this.antal + ", " + "Pris: " + this.pris
				+ " ]";
	}

}
