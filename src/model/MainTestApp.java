package model;

import java.time.LocalDate;

public class MainTestApp {
	public static void main(String[] args) {
		Salg salg = new Salg(new ProcentRabat());
		System.out.println("%: " + salg.useStrategy(50, 20));
		
		salg = new Salg(new PrisRabat());
		System.out.println("pri: " + salg.useStrategy(50, 20));
		
		salg = new Salg(new IngenRabat());
		System.out.println("00: " + salg.useStrategy(50, 20));
		
		Kunde k1 = new Kunde(1, "A B", "Vej 1", 12345678, "ab@hotmail.com");
		Kunde k2 = new Kunde(2, "C D", "Vej 2", 23456789, "cd@hotmail.com");
		Kunde k3 = new Kunde(3, "E F", "Vej 3", 34567890, "ef@hotmail.com");
		Kunde k4 = new Kunde(4, "G H", "Vej 4", 45678901, "gh@hotmail.com");
		Kunde k5 = new Kunde(5, "I J", "Vej 5", 56789012, "ij@hotmail.com");
		
		Udlejning u1 = new Udlejning(LocalDate.parse("2019-10-21"), LocalDate.parse("2019-11-10"), k1);
		Udlejning u2 = new Udlejning(LocalDate.parse("2019-10-22"), LocalDate.parse("2019-11-11"), k2);
		Udlejning u3 = new Udlejning(LocalDate.parse("2019-10-23"), LocalDate.parse("2019-11-12"), k3);
		Udlejning u4 = new Udlejning(LocalDate.parse("2019-10-24"), LocalDate.parse("2019-11-13"), k4);
		Udlejning u5 = new Udlejning(LocalDate.parse("2019-10-25"), LocalDate.parse("2019-11-14"), k5);
		
		//String navn, Enhed enhed
		ProduktGruppe pg1 = new ProduktGruppe("Flaske", Enhed.CL);
		ProduktGruppe pg2 = new ProduktGruppe("Fadøl", Enhed.LITER);
		
		//String navn, int størrelse, ProduktGruppe produktgruppe
		Produkt p1 = new Produkt("Påskebryg", 60, pg1);
		Produkt p2 = new Produkt("Øl2", 60, pg1);
		Produkt p3 = new Produkt("Øl3", 1, pg2);
		Produkt p4 = new Produkt("Øl4", 60, pg1);
		Produkt p5 = new Produkt("Øl5", 1, pg2);
		
		System.out.println(u1.toString());
		System.out.println(u2.toString());
		System.out.println(u3.toString());
		System.out.println(u4.toString());
		System.out.println(u5.toString());
		System.out.println();
		
		System.out.println(u1.getStartDato());
		System.out.println(u1.getSlutDato());
		
		u1.setAfleveret(true);
		
		System.out.println(u1.isAfleveret());
		
		System.out.println();
		
		u1.createSalgslinje(p1, 2, 80);
	}
}
