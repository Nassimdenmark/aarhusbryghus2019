package model;

import java.time.DateTimeException;
import java.time.LocalDate;

/**
 * Udlejning extender salg da vi i dette system betragter udlejning som et salg
 * Dette betyder at udlejning kan bruge metoder m.m. fra salg
 *
 */
public class Udlejning extends Salg {
	private LocalDate startDato, slutDato;
	private boolean afleveret;
	private double pantPris;
	private Kunde kunde;

	public Udlejning(LocalDate startDato, LocalDate slutDato, Kunde kunde) {
		super();
		this.startDato = startDato;
		this.slutDato = slutDato;
		this.kunde = kunde;
		setAfleveret(false);
	}

	// ------------------GETTERS---------------------

	public LocalDate getStartDato() {
		return startDato;
	}

	public LocalDate getSlutDato() {
		return slutDato;
	}

	public double getPantPris() {
		return pantPris;
	}

	// ------------------SETTERS---------------------

	public void setStartDato(LocalDate startDato) {
		if (startDato.isAfter(slutDato) || startDato.isBefore(LocalDate.now())) {
			throw new DateTimeException("The date you picked is incorrect, please pick another.");
		} else {
			this.startDato = startDato;
		}
	}

	public void setSlutDato(LocalDate slutDato) {
		if (slutDato.isBefore(startDato) || slutDato.isBefore(LocalDate.now())) {
			throw new DateTimeException("The date you picked is incorrect, please pick another.");
		} else {
			this.slutDato = slutDato;
		}
	}

	public void setAfleveret(boolean afleveret) {
		this.afleveret = afleveret;
	}

	// ------------------ANDRE METODER---------------------

	public boolean isAfleveret() {
		return afleveret;
	}

	@Override
	public String toString() {
		return kunde + " " + startDato + " til " + slutDato;
	}
}
