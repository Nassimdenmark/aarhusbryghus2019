package model;

public class Kunde {
	private int kundeID;
	private String navn;
	private String adresse;
	private int telefonnr;
	private String email;

	public Kunde(int kundeID, String navn, String adresse, int telefonnr, String email) {
		this.kundeID = kundeID;
		this.navn = navn;
		this.adresse = adresse;
		this.telefonnr = telefonnr;
		this.email = email;
	}

	@Override
	public String toString() {
		return "ID = " + kundeID + ", " + (navn != null ? navn : "") + "]";
	}

	// -----------------------GETTERS------------------------------

	public int getKundeID() {
		return kundeID;
	}

	public String getNavn() {
		return navn;
	}

	public String getAdresse() {
		return adresse;
	}

	public int getTelefonnr() {
		return telefonnr;
	}

	public String getEmail() {
		return email;
	}

	// -----------------------SETTERS------------------------------

	public void setKundeID(int kundeID) {
		this.kundeID = kundeID;
	}

	public void setNavn(String navn) {
		this.navn = navn;
	}

	public void setAdresse(String adresse) {
		this.adresse = adresse;
	}

	public void setTelefonnr(int telefonnr) {
		this.telefonnr = telefonnr;
	}

	public void setEmail(String email) {
		this.email = email;
	}
}
