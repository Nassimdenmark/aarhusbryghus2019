package storage;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;
import model.Produkt;
import model.ProduktGruppe;
import model.Udlejning;

public class Storage {
	private final Set<Produkt> produkter = new TreeSet<>(); // 'Set' = ingen dubletter
	private final Set<ProduktGruppe> produktgrupper = new TreeSet<>(); // 'TreeSet' = auto sortering i a-z orden
	private final List<Udlejning> udlejninger = new ArrayList<>(); // Hurtig og effektiv; kan have dubletter
	private static Storage storage;

	/**
	 * Storage laves her til en Singleton
	 */
	public static Storage getInstance() {
		if (storage == null) {
			storage = new Storage();
		}
		return storage;
	}

	// -------------GET METODER-----------------------------------

	public Set<Produkt> getProdukter() {
		return new TreeSet<>(produkter);
	}

	public Set<ProduktGruppe> getProduktgrupper() {
		return new TreeSet<>(produktgrupper);
	}

	public List<Udlejning> getUdlejninger() {
		return new ArrayList<>(udlejninger);
	}

	// -------------ADD METODER-----------------------------------

	public void addProdukt(Produkt p) {
		produkter.add(p);
	}

	public void addProduktGruppe(ProduktGruppe pg) {
		produktgrupper.add(pg);
	}

	public void addUdlejning(Udlejning u) {
		if (!udlejninger.contains(u)) {
			udlejninger.add(u);
		}
	}

	// -------------REMOVE METODER-----------------------------------

	public void removeProdukt(Produkt p) {
		if (produkter.contains(p)) {
			produkter.remove(p);
		}
	}

	public void removeProduktGruppe(ProduktGruppe pg) {
		if (produktgrupper.contains(pg)) {
			produktgrupper.remove(pg);
		}
	}

	public void removeUdlejning(Udlejning u) {
		if (udlejninger.contains(u)) {
			udlejninger.remove(u);
		}
	}
}
