package controller;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import model.Enhed;
import model.Kunde;
import model.Produkt;
import model.ProduktGruppe;
import model.Udlejning;
import storage.Storage;

public class Controller {
	private static Storage storage;
	private static Controller controller;

	public static void initializeStorage() {
		storage = Storage.getInstance();
	}

	/**
	 * Laver controller til en Singleton
	 */
	public static Controller getController() {
		if (controller == null) {
			controller = new Controller();
		}
		return controller;
	}

	// -------------------CREATE METODER----------------------

	/**
	 * Laver en ny produktgruppe og tilføjer den til storage
	 * 
	 * @param s
	 * @param e
	 * @return pg, den nu oprettet produktgruppe
	 */
	public ProduktGruppe createProduktGruppe(String s, Enhed e) {
		ProduktGruppe pg = new ProduktGruppe(s, e);
		storage.addProduktGruppe(pg);
		return pg;
	}

	/**
	 * Laver et nyt produkt og tilføjer den til storage
	 * 
	 * @param s
	 * @param i
	 * @param pg
	 * @return p, det nu oprettet produkt
	 */
	public Produkt createProdukt(String s, int i, ProduktGruppe pg) {
		Produkt p = new Produkt(s, i, pg);
		storage.addProdukt(p);
		return p;
	}

	/**
	 * Laver en ny udlejning og tilføjer den til storage
	 * 
	 * @param start
	 * @param slut
	 * @param k
	 * @return u, den nu oprettet udlejning
	 */
	public Udlejning createUdlejning(LocalDate start, LocalDate slut, Kunde k) throws RuntimeException {
		if (start == null || slut == null) {
			throw new RuntimeException("Vælg venligst dato");
		}
		if (start.isAfter(slut) || slut.isBefore(start)) {
			throw new RuntimeException("Start dato skal være FØR slut dato");
		}
		Udlejning u = new Udlejning(start, slut, k);
		storage.addUdlejning(u);
		return u;
	}

	// -------------------GET METODER----------------------

	/**
	 * Henter Set med alle produktgrupper i systemet 'Set' = ingen dubletter
	 * 
	 * @return Set
	 */
	public Set<ProduktGruppe> getProduktGrupper() {
		return storage.getProduktgrupper();
	}

	/**
	 * Henter Set med alle produkter i systemet 'Set' = ingen dubletter
	 * 
	 * @return Set
	 */
	public Set<Produkt> getProdukter() {
		return storage.getProdukter();
	}

	/**
	 * Henter List med alle udlejninger i systemet
	 * 
	 * @return List
	 */
	public List<Udlejning> getUdlejninger() {
		return storage.getUdlejninger();
	}

	/**
	 * Laver arraylist med alle udlejninger som endnu ikke er afleveret
	 * 
	 * @return ikkeAfleveret
	 */
	public List<Udlejning> getIkkeAfleveretUdlejninger() {
		List<Udlejning> ikkeAfleveret = new ArrayList<>();
		for (Udlejning u : storage.getUdlejninger()) {
			if (!u.isAfleveret()) {
				ikkeAfleveret.add(u);
			}
		}
		return ikkeAfleveret;
	}

	/**
	 * Laver en arraylist med alle udlejninger med start på given dato
	 * 
	 * @param dato
	 * @return list
	 */
	public List<Udlejning> getUdlejningerPrDato(LocalDate dato) {
		List<Udlejning> list = new ArrayList<>();
		for (Udlejning u : storage.getUdlejninger()) {
			if (u.getStartDato().equals(dato)) {
				list.add(u);
			}
		}
		return list;
	}

	// -------------------ADD METODER----------------------

	/**
	 * Tilføjer et produkt p til en produktgruppe pg
	 * 
	 * @param pg
	 * @param p
	 */
	public void addProduktTilProduktGruppe(ProduktGruppe pg, Produkt p) {
		pg.addProdukt(p);
	}

	// -------------------REMOVE METODER----------------------
	// TODO: remove metode til produktgrupper? Hvad skal ske med produkter i
	// gruppen?

	/**
	 * Fjerner et produkt fra en produktgruppe
	 * 
	 * @param pg
	 * @param p
	 * @throws RuntimeException
	 */
	public void removeProdukt(ProduktGruppe pg, Produkt p) throws RuntimeException {
		if (pg == null) {
			throw new RuntimeException("Ugyldig produktgruppe");
		}
		if (p == null) {
			throw new RuntimeException("Ugyldigt produkt");
		}
		storage.removeProdukt(p);
	}
}
