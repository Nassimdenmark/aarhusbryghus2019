package diverse;

public class Pris {
	private int pris;
	private Produkt produkt;

	public Pris(Produkt produkt, int pris) {
		this.produkt = produkt;
		this.pris = pris;
	}

	public void addPris(int pris) {
		this.pris = pris;
	}

	public int getPris() {
		return this.pris;
	}

	public Produkt getProdukt() {
		return this.produkt;
	}

	@Override
	public String toString() {
		return this.produkt + ", Pris " + this.pris;
	}

}
