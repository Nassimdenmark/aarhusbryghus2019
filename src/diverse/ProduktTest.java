package diverse;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertSame;

import org.junit.Test;

public class ProduktTest {
	ProduktGruppe flaske = new ProduktGruppe("Flaske", Enhed.CL);

	@Test
	public void Produkt() {

		Produkt p1 = new Produkt("Klosterbryg", 60, this.flaske);

		assertNotNull(this.flaske);
		assertNotNull(this.flaske.getProdukter().contains(p1));
		assertSame(p1.getProduktgruppe(), this.flaske);
		assertEquals(60, p1.getStørrelse());
		assertEquals("Klosterbryg", p1.getNavn());
		assertEquals(p1.getProduktgruppe().getEnhed(), Enhed.CL);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testStørrelseUnder1() {
		Produkt p1 = new Produkt("Klosterbryg", 0, this.flaske);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testStørrelseOver100() {
		Produkt p2 = new Produkt("Klosterbryg", 101, this.flaske);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testNavnIkkeBogstav() {
		Produkt p2 = new Produkt("4", 60, this.flaske);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testNavnUnder3Karaktere() {
		Produkt p2 = new Produkt("Kl", 60, this.flaske);
	}

	@Test(expected = NullPointerException.class)
	public void testIngenProduktGruppe() {
		Produkt p2 = new Produkt("Klosterbryg", 60, null);
	}
}
