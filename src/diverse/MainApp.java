package diverse;

public class MainApp {
	public static void main(String[] args) {
		ProduktGruppe flaske = new ProduktGruppe("Flaske", Enhed.CL);

		Produkt p1 = new Produkt("Klosterbryg", 60, flaske);

		Salgssituation butik = new Salgssituation("Butik");
		Pris pris1 = butik.createPris(p1, 33);
		flaske.addProdukt(p1);

		Salgssituation fredagsbar = new Salgssituation("Fredagsbar");
		fredagsbar.createPris(p1, 99);

		System.out.println("Butik: " + butik.getPriser());
		System.out.println("FredagsBar: " + fredagsbar.getPriser());
		butik.removePris(pris1);
		System.out.println("Butik efter remove " + butik.getPriser());
		System.out.println(p1.getProduktgruppe());
		flaske.removeProdukt(p1);
		System.out.println(p1.getProduktgruppe());
	}
}
