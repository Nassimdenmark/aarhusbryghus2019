package diverse;

public class Produkt {
	private String navn;
	private int størrelse;
	private ProduktGruppe produktgruppe;

	public Produkt(String navn, int størrelse, ProduktGruppe produktgruppe) {
		if (navn.length() < 3)
			throw new IllegalArgumentException("Produktnavn skal være på MIN 3 bogstaver: " + navn);
		if (!navn.matches("[a-zA-Z_]+"))
			throw new IllegalArgumentException("Produktnavn må KUN indeholde bogstaver");
		this.navn = navn;
		//		if (produktgruppe == null)
		//			throw new NullPointerException("Produktgruppe må ikke være NULL");
		Pre.require(produktgruppe != null);
		this.produktgruppe = produktgruppe;
		if (størrelse < 1 || størrelse > 100)
			throw new IllegalArgumentException("Størrelsen SKAL være mellem 1 - 100: " + størrelse);
		this.størrelse = størrelse;
	}

	public String getNavn() {
		return this.navn;
	}

	public ProduktGruppe getProduktgruppe() {
		return this.produktgruppe;
	}

	public int getStørrelse() {
		return this.størrelse;
	}

	@Override
	public String toString() {
		return this.navn + ", " + this.størrelse + " " + this.produktgruppe.getEnhed();
	}

	public void setProduktGruppe(ProduktGruppe produktGruppe) {
		if (this.produktgruppe != produktGruppe) {
			this.produktgruppe = produktGruppe;
		}
	}

	public void setProduktGruppeNull() {
		if (this.produktgruppe != null) {
			ProduktGruppe oldproduktgruppe = this.produktgruppe;
			this.produktgruppe = null;
			oldproduktgruppe.removeProdukt(this);
		}
	}

}
