package diverse;

public class PantProdukt extends Produkt {
	private int pantPris;
	private ProduktGruppe produktgruppe;

	public PantProdukt(String navn, int størrelse, int pantPris, ProduktGruppe produktgruppe) {
		super(navn, størrelse, produktgruppe);
		this.pantPris = pantPris;
	}

	public int getPantPris() {
		return this.pantPris;
	}

	@Override
	public ProduktGruppe getProduktgruppe() {
		return this.produktgruppe;
	}

	@Override
	public String toString() {
		return "PantPris " + this.pantPris + "";
	}

}
