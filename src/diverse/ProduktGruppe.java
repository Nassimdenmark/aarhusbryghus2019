package diverse;

import java.util.ArrayList;

public class ProduktGruppe {
	private String navn;
	private Enhed enhed;
	private ArrayList<Produkt> produkter = new ArrayList<>();

	public ProduktGruppe(String navn, Enhed enhed) {
		this.navn = navn;
		this.enhed = enhed;
	}

	public void addProdukt(Produkt produkt) {
		if (!this.produkter.contains(produkt)) {
			this.produkter.add(produkt);
			produkt.setProduktGruppe(this);
		}
	}

	public void removeProdukt(Produkt produkt) {
		if (this.produkter.contains(produkt)) {
			this.produkter.remove(produkt);
			produkt.setProduktGruppeNull();
		}
	}

	public String getNavn() {
		return this.navn;
	}

	public Enhed getEnhed() {
		return this.enhed;
	}

	public ArrayList<Produkt> getProdukter() {
		return new ArrayList<>(this.produkter);
	}

	@Override
	public String toString() {
		return this.navn;
	}

}
