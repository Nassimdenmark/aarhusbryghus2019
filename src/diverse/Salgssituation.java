package diverse;

import java.util.ArrayList;

public class Salgssituation {
	private String navn;
	private ArrayList<Pris> prisProdukt = new ArrayList<>();

	public Salgssituation(String navn) {
		this.navn = navn;
	}

	public Pris createPris(Produkt produktet, int pris) {
		Pris prisProdukt = new Pris(produktet, pris);
		if (!this.prisProdukt.contains(prisProdukt)) {
			this.prisProdukt.add(prisProdukt);
		}
		return prisProdukt;
	}

	public void removePris(Pris produkt) {
		if (this.prisProdukt.contains(produkt)) {
			this.prisProdukt.remove(produkt);
		}
	}

	public ArrayList<Pris> getPriser() {
		return new ArrayList<>(this.prisProdukt);
	}
	
	public String getNavn() {
		return this.navn;
	}

	@Override
	public String toString() {
		return "Salgssituation [navn=" + this.navn + ", priser=" + this.prisProdukt + "]\n";
	}

}
